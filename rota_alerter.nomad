job "${PREFIX}_rota_alerter" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_rota_alerter" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/rota_alerter/rota_alerter:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_rota_alerter"
        }
      }
      volumes = [
        "${DB_MOUNTPOINT}:/state",
        "/etc/nomad/${MM_HOOK}:/secrets/mm_hook",
      ]
    }

    env {
      NOMAD_ADDR = "$NOMAD_ADDR"
      ROTAS = "$ROTAS"
      TAG = "${PREFIX}_rota_alerter"
    }

    resources {
      cpu = 1000 # Mhz
      memory = 256 # MB
    }

  }
}
