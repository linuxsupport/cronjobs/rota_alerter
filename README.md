# rota_alerter

Most modifications will be done to `/rota_alerter/prod.rotas.yml`, which is where you can set up
what rotas (from https://op-webtools.web.cern.ch/planning/) to look for, where to post the notification
and what message to send. Check that file for details on the format.
